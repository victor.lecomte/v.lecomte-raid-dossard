// Librairies
#include <iostream>
#include <string>
#include "../header/integrate_team.h"

// Default constructor
Integrate_Team::Integrate_Team() :
  team_number(12345),
  raid_code("2020TF"),
  license("VT990"),
  individual_time(60),
  bib_number("1-2020TF-VL-1")
{
  std::cout << "The default constructor is composed as follows" << std::endl;
  std::cout << "The team number is : " << team_number << std::endl;
  std::cout << "The raid code is : " << raid_code << std::endl;
  std::cout << "The license is : " << license << std::endl;
  std::cout << "The individual time is : " << individual_time << std::endl;
  std::cout << "The bib number is : " << bib_number << std::endl;
}

// Constructor with parameter
Integrate_Team::Integrate_Team(int _team_number, std::string _raid_code, std::string _license, int _individual_time, std::string _bib_number) : 
  team_number(_team_number),
  raid_code(_raid_code),
  license(_license),
  individual_time(_individual_time),
  bib_number(_bib_number)
{
  std::cout << "The constructor with parameter is composed as follows " << std::endl;
  std::cout << "The team number is : " << team_number << std::endl;
  std::cout << "The raid code is : " << raid_code << std::endl;
  std::cout << "The license is : " << license << std::endl;
  std::cout << "The individual time is : " << individual_time << std::endl;
  std::cout << "The bib number is : " << bib_number << std::endl; 
}

// Destructive
Integrate_Team::~Integrate_Team() {}

// Accessors (getters)
int Integrate_Team::getTeam_Number()
{
 return team_number;
}

std::string Integrate_Team::getRaid_Code()
{
  return raid_code;
}

std::string Integrate_Team::getLicense()
{
 return license;
}

int Integrate_Team::getIndividual_Time()
{
 return individual_time;
}

std::string Integrate_Team::getBib_Number()
{
 return bib_number;
}
    
// Mutators (setters)
void Integrate_Team::setTeam_Number(int _team_number)
{
 team_number = _team_number;
}

void Integrate_Team::setRaid_Code(std::string _raid_code)
{
 raid_code = _raid_code;
}

void Integrate_Team::setLicense(std::string _license)
{
 license = _license;
}

void Integrate_Team::setIndividual_Time(int _individual_time)
{
 individual_time = _individual_time;
}

void Integrate_Team::setBib_Number(std::string _bib_number)
{
 bib_number = _bib_number;
}


