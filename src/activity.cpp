// Librairies
#include <iostream>
#include <string>
#include "../header/activity.h"

// Default constructor
Activity::Activity() :
  raid_code("2020F"),
  pamphlets("Tour de France 2020")
{
  std::cout << "The default constructor is composed as follows" << std::endl;
  std::cout << "The raid code is : " << raid_code << std::endl;
  std::cout << "The pamphlets is : " << pamphlets << std::endl;
}

// Default constructor
Activity::Activity(std::string _raid_code, std::string _pamphlets) :
  raid_code(_raid_code),
  pamphlets(_pamphlets)
{
  std::cout << "The constructor with parameter is composed as follows " << std::endl;
  std::cout << "The raid_code is: " << raid_code << std::endl;                        
  std::cout << "The pamphlets is : " << pamphlets << std::endl; 
}

// Destructive
Activity::~Activity() {}

// Accessors (getters)
std::string Activity::getRaid_Code()
{
  return raid_code;
}

std::string Activity::getPamphlets()
{
  return pamphlets;
}

// Mutatore (setters)
void Activity::setRaid_Code(std::string _raid_code)
{
  raid_code = _raid_code;
}

void Activity::setPamphlets(std::string _pamphlets)
{
  pamphlets = _pamphlets;
}


