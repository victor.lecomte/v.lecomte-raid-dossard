// Librairies
#include <iostream>
#include <string>
#include "../header/raid_activity.h"

// Default constructor
Raid_Activity::Raid_Activity() :
  raid_code("2020TF"),
  activity_code("rbike")
{
  std::cout << "The default constructor is composed as follows" << std::endl;
  std::cout << "The raid code is : " << raid_code << std::endl;
  std::cout << "The activity_code is : " << activity_code << std::endl;
}

// Default constructor
Raid_Activity::Raid_Activity(std::string _raid_code, std::string _activity_code) :
  raid_code(_raid_code),
  activity_code(_activity_code)
{
  std::cout << "The constructor with parameter is composed as follows " << std::endl;
  std::cout << "The raid_code is: " << raid_code << std::endl;                        
  std::cout << "The activity_code is : " << activity_code << std::endl; 
}

// Destructive
Raid_Activity::~Raid_Activity() {}

// Accessors (getters)
std::string Raid_Activity::getRaid_Code()
{
  return raid_code;
}

std::string Raid_Activity::getActivity_Code()
{
  return activity_code;
}

// Mutatore (setters)
void Raid_Activity::setRaid_Code(std::string _raid_code)
{
  raid_code = _raid_code;
}

void Raid_Activity::setActivity_Code(std::string _activity_code)
{
  activity_code = _activity_code;
}


