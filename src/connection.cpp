#include "../header/connection.h"

#include <iostream>

Connection::Connection() :
  Connection("postgresql.bts-malraux72.net",
             5432,
             "v.lecomte",
             "v.lecomte")
{}

Connection::Connection(std::string host,
                       int16_t port,
                       std::string user,
                       std::string dbname) :
  _handler(nullptr),
  _host(host),
  _port(port),
  _user(user),
  _dbname(dbname),
  _connexion_infos("connect_timeout=5"),
  _go_exec(false)
{}

Connection::~Connection()
{
  if(_handler != nullptr)
  {
    PQfinish(_handler);
  }
}

void Connection::host(std::string host)
{
  _host = host;
}

std::string Connection::host()
{
  return _host;
}

void Connection::port(int16_t port)
{
  _port = port;
}

int16_t Connection::port()
{
  return _port;
}

void Connection::user(std::string user)
{
  _user = user;
}

std::string Connection::user()
{
  return _user;
}

void Connection::dbname(std::string dbname)
{
  _dbname = dbname;
}

std::string Connection::dbname()
{
  return _dbname;
}


void Connection::init()
{
  _connexion_infos += " host=" + _host;
  _connexion_infos += " port=" + std::to_string(_port);
  _connexion_infos += " user=" + _user;
  _connexion_infos += " dbname=" + _dbname;
}

bool Connection::connect()
{
  init();
  std::cout << _connexion_infos << std::endl;

  if(PQping(_connexion_infos.c_str()) == PQPING_OK)
  {
    std::cout << "La connexion au serveur de base de données est possible." << std::endl;
    _handler = PQconnectdb(_connexion_infos.c_str());

    if(PQstatus(_handler) == CONNECTION_OK)
    {
      std::cout << "La connexion au serveur de base de données a été établie avec succès." << std::endl;
      _go_exec = true;
    }
    else
    {
      std::cerr << "Malheureusement la connexion a échoué. Vérifiez les permissions." << std::endl;
    }
  }
  else
  {
    std::cerr << "Malheureusement le serveur n'est pas joignable. Vérifiez la connectivité." << std::endl;
  }

  return _go_exec;
}

PGresult *Connection::exec(std::string request)
{
  if(connect())
  {
    PGresult *res = PQexec(_handler, request.c_str());
    ExecStatusType etat_execution = PQresultStatus(res);

    if(etat_execution == PGRES_TUPLES_OK)
    {
      return res;
    }
    else if(etat_execution == PGRES_EMPTY_QUERY)
    {
      std::cerr << "La chaîne envoyée au serveur était vide." << std::endl;
    }
    else if(etat_execution == PGRES_COMMAND_OK)
    {
      std::cerr << "La requête SQL n'a renvoyé aucune donnée." << std::endl;
    }
    else if(etat_execution == PGRES_COPY_OUT)
    {
      std::cerr << "Début de l'envoi (à partir du serveur) d'un flux de données." << std::endl;
    }
    else if(etat_execution == PGRES_COPY_IN)
    {
      std::cerr << "Début de la réception (sur le serveur) d'un flux de données." << std::endl;
    }
    else if(etat_execution == PGRES_BAD_RESPONSE)
    {
      std::cerr << "La réponse du serveur n'a pas été comprise." << std::endl;
    }
    else if(etat_execution == PGRES_NONFATAL_ERROR)
    {
      std::cerr << "Une erreur non fatale (une note ou un avertissement) est survenue." << std::endl;
    }
    else if(etat_execution == PGRES_FATAL_ERROR)
    {
      std::cerr << "Une erreur fatale est survenue." << std::endl;
    }
    else if(etat_execution == PGRES_COPY_BOTH)
    {
      std::cerr << "Lancement du transfert de données Copy In/Out (vers et à partir du serveur)." << std::endl;
    }
    else if(etat_execution == PGRES_SINGLE_TUPLE)
    {
      std::cerr << "La structure PGresult contient une seule ligne de résultat provenant de la commande courante." << std::endl;
    }
  }

  return nullptr;
}
