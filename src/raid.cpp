// Librairies
#include <iostream>
#include <string>
#include "../header/raid.h"

// Defaullt constructor
Raid::Raid() :
  code("2020TF"),
  name("Tour de France 2020"),
  start_date("11/05/2020"),
  town("Le Mans"),
  region("Pays de la Loire"),
  nb_max_per_team(6),
  registration_amount(2500.00),
  nb_women(1),
  maxi_time(2880),
  age_minimum(18)
{
 std::cout << "The default constructor is composed as follows" << std::endl;
 std::cout << "The code is : " << code << std::endl;
 std::cout << "The raid name  id : " << name << std::endl;
 std::cout << "The star date of the raid is : " << start_date << std::endl;
 std::cout << "the town is : " << town << std::endl;
 std::cout << "The region is : " << region << std::endl;
 std::cout << "The maximum number per team is : " << nb_max_per_team << std::endl;
 std::cout << "The registration fee is :  " << registration_amount << std::endl;
 std::cout << "The number of women is : " << nb_women << std::endl;
 std::cout << "The maximum time is : " << maxi_time << std::endl;
 std::cout << "The age minimum is : " << age_minimum << std::endl;
}

// Constructor with parameters
Raid::Raid(std::string _code, std::string _name, std::string _start_date, std::string _town, std::string _region, int _nb_max_per_team, float _registration_amount, int _nb_women, int _maxi_time, int _age_minimum) :
  code(_code),
  name(_name),
  start_date(_start_date),
  town(_town),
  region(_region),
  nb_max_per_team(_nb_max_per_team),
  registration_amount(_registration_amount),
  nb_women(_nb_women),
  maxi_time(_maxi_time),
  age_minimum(_age_minimum)
{
  std::cout <<  "The constructor with parameter is composed as follows "  << std::endl;
  std::cout << "The code is : " << code << std::endl;
  std::cout << "The raid name  id : " << name << std::endl;
  std::cout << "The star date of the raid is : " << start_date << std::endl;
  std::cout << "the town is : " << town << std::endl;
  std::cout << "The region is : " << region << std::endl;
  std::cout << "The maximum number per team is : " << nb_max_per_team << std::endl;
  std::cout << "The registration fee is :  " << registration_amount << std::endl;
  std::cout << "The number of women is : " << nb_women << std::endl;
  std::cout << "The maximum time is : " << maxi_time << std::endl;
  std::cout << "The age minimum is : " << age_minimum << std::endl;
}  

  
// Destructive
  Raid::~Raid() {}

// Accessors (getters)
std::string Raid::getCode()
{
  return code;
}

std::string Raid::getName()
{
  return name;
}

std::string Raid::getStart_Date()
{
  return start_date;
}

std::string Raid::getTown()
{
  return town;
}

std::string Raid::getRegion()
{
  return region;
}

int Raid::getNb_Max_Per_Team()
{
  return nb_max_per_team;
}

float Raid::getRegistration_Amount()
{
  return registration_amount;
}

int Raid::getNb_Women()
{
  return nb_women;
}

int Raid::getMaxi_Time()
{
  return maxi_time;
}

int Raid::getAge_Minimum()
{
  return age_minimum;
}

// Mutators (setters)
void Raid::setCode(std::string _code)
{
  code = _code;
}

void Raid::setName(std::string _name)
{
  name = _name;
}

void Raid::setStart_Date(std::string _start_date)
{
  start_date = _start_date;
}
void Raid::setTown(std::string _town)
{
  town = _town;
}

void Raid::setRegion(std::string _region)
{
  region = _region;
}

void Raid::setNb_Max_Per_Team(int _nb_max_per_team)
{
  nb_max_per_team = _nb_max_per_team;
}

void Raid::setRegistration_Amount(float _registration_amount)
{
  registration_amount = _registration_amount;
}

void Raid::setNb_Women(int _nb_women)
{
  nb_women = _nb_women;
}

void Raid::setMaxi_Time(int _maxi_time)
{
  maxi_time = _maxi_time;
}

void Raid::setAge_Minimum(int _age_minimum)
{
  age_minimum = _age_minimum;
}
