-- insertions dans la table activite :
insert into "raid_dossard".activite values(1,'Vélo de route');
insert into "raid_dossard".activite values(2,'Basketball');
insert into "raid_dossard".activite values(3,'Football');
insert into "raid_dossard".activite values(4,'Handball');
insert into "raid_dossard".activite values(5,'Volleyball');
insert into "raid_dossard".activite values(6,'Natation');
insert into "raid_dossard".activite values(7,'Saut à la perche');

-- insertions dans la table raid :
insert into "raid_dossard".raid (code, nom, date_debut,ville,region,nb_maxi_par_equipe, montant_inscription,nb_femmes,duree_maxi,age_minimum) values
('A02BA','Euroleague','10/06/2020','Allonnes','Pays De La Loire',5,10.00,1,90,14),
('A03FO','Liga','27/07/2000','Le Mans','Pays De La Loire',4,18.90,1,90,18);

-- insertions dans la table activite_raid :
insert into "raid_dossard".activite_raid values('A03FO','1');
insert into "raid_dossard".activite_raid values('A02BA','2');

-- insertions dans la table coureur :
insert into "raid_dossard".coureur values('L1VL','Lecomte','Victor','M','Francais','vl@gmail.com',true,'27-05-1999');
insert into "raid_dossard".coureur values('L2ML','Messi','Lionel','M','Espagnol','ml@gmail.com',true,'24-06-1987');
insert into "raid_dossard".coureur values('L3MJ','Johannès','Marine','F','Française','mj@gmail.com',true,'21-02-1995');
insert into "raid_dossard".coureur values('L4DC','Dumerc','Celine','F','Française','dc@gmail.com',true,'09-07-1982');
insert into "raid_dossard".coureur values('L5LSE','Le Sommer','Eugénie','F','Française','lse@gmail.com',true,'18-05-1989');
insert into "raid_dossard".coureur values('L6GA','Griezmann','Antoine','M','Francais','ga@gmail.com',true,'21-03-1991');
insert into "raid_dossard".coureur values('L7JM','Jordan','Michael','M','Etasunien','jm@gmail.com',true,'17-02-1963');
insert into "raid_dossard".coureur values('L8BK','Bryant','Kobe','M','Etasunien','kb@gmail.com',true,'23-08-1978');
insert into "raid_dossard".coureur values('L9BG','Bryant','Gianna Maria-Onore','F','Etasunienne','bg@gmail.com',true,'01-05-2006');
insert into "raid_dossard".coureur values('L10RC','Ronaldo','Christiano','H','Espagnol','rc@gmail.com',true,'05-09-1989');

-- insertions dans la table equipe :
insert into "raid_dossard".equipe (numero,code_raid,nom) values('1','A02BA','Le Mans Sarthe Basket');
insert into "raid_dossard".equipe (numero,code_raid,nom) values('2','A03FO','FC Barcelone');

-- insertions dans la table integrer_equipe :
insert into "raid_dossard".integrer_equipe (numero_equipe,code_raid,licence) values('1','A02BA','L1VL');
insert into "raid_dossard".integrer_equipe (numero_equipe,code_raid,licence) values('1','A02BA','L4DC');
insert into "raid_dossard".integrer_equipe (numero_equipe,code_raid,licence) values('1','A02BA','L8BK');
insert into "raid_dossard".integrer_equipe (numero_equipe,code_raid,licence) values('1','A02BA','L7JM');
insert into "raid_dossard".integrer_equipe (numero_equipe,code_raid,licence) values('1','A02BA','L9BG');
insert into "raid_dossard".integrer_equipe (numero_equipe,code_raid,licence) values('2','A03FO','L5LSE');
insert into "raid_dossard".integrer_equipe (numero_equipe,code_raid,licence) values('2','A03FO','L10RC');
insert into "raid_dossard".integrer_equipe (numero_equipe,code_raid,licence) values('2','A03FO','L2ML');
insert into "raid_dossard".integrer_equipe (numero_equipe,code_raid,licence) values('2','A03FO','L6GA');

