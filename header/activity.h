#ifndef ACTIVITY_H
#define ACTIVITY_H

// Librairies
#include <iostream>
#include <string>

/**  
 * \file activity.h    
 * \brief definition of activity class        
 * \version 0.0.1    
 * \author LECOMTE Victor     
 */
class Activity
{

private : 
  std::string code; ///< code as string
  std::string pamphlets; ///< pamphlets as string

public : 
// Default constructor
  /**
   * \brief Default constructor
   */
   Activity();
  
// Constructor with parameters
  /**
   * \brief Constructors with differents parameters
   * \param code - the raid_code number of an activity
   * \param pamphlets - the pamphlets of the activity
   */
   Activity(std::string code, std::string pamphlets);

// Destructive
  /**
   * \brief Default destructor of an activity 
   */
  ~Activity();

 // Accessors (getters)
  /**
   * \brief Allows the recovery of the code 
   * \return string representing the code of an activity
   */
   std::string getCode();
  
  /**   
   * \brief Allows reading a pamphlets of an activity 
   * \return string representing the pamphlets of an activity
   */
   std::string getPamphlets();

// Mutators (setters)
  /**
   * \brief Allows the modification of the code
   */
   void setCode(std::string);

  /**               
   * \brief Allows the modification of pamphlets         
   */
   void setPamphlets(std::string);

// Methods
  /**
   *  Allows the display of all parameters (attributes)
   */
  std::string toString();
};
#endif // ACTIVITY_H
