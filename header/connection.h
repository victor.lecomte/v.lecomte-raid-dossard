#ifndef CONNECTION_H
#define CONNECTION_H

#include <libpq-fe.h>

#include <string>

/**
 * \file connection.h
 * \brief Represent the connection of the databaase
 * \version 0.0.1
 * \author LECOMTE Victor
 * \author DAVID Grégory
 */
class Connection
{
 public:
  explicit Connection();
  explicit Connection(std::string, int16_t, std::string, std::string);
  ~Connection();
  
  void host(std::string);
  std::string host();
  
  void port(int16_t);
  int16_t port();
  
  void user(std::string);
  std::string user();
  
  void dbname(std::string);
  std::string dbname();

  void init();
  bool connect();
  PGresult *exec(std::string);

 private:
  PGconn *_handler;
  std::string _host;
  int16_t _port;
  std::string _user;
  std::string _dbname;
  std::string _connexion_infos;
  bool _go_exec;
};
#endif // CONNECTION_H
