#ifndef TEAM_H
#define TEAM_H

// Librairies
#include <iostream>
#include <chrono>
#include <string>
#include <cstdint>

/**
 * \file team.h
 * \brief definition of team class
 * \version 0.0.1
 * \author LECOMTE Victor  
 */
class Team
{

private :
  int number; ///< number as int
  std::string raid_code; ///< raid_code as string
  std::string name; ///< name as string
  std::chrono::system_clock record_date; ///< record_date as date
  int global_time = 0; ///< global_time as int, initialized at 0
  int ranking = 0; ///< ranking as int, initialized at 0 
  int penalites_bonif = 0; ///< penalites_bonif as int, initialized at 0 
 
public : 
// Default constructor
  /**
   * \brief Default constructor
   */
  Team();

// Constructor with parameters
  /**
   * \brief Constructors with differents parameters
   * \param number - the number of a team 
   * \param raid_code - the raid_code of a team
   * \param name - the name of a team
   * \param record_date - the record_date of a team
   * \param ranking - the ranking of a team
   * \param global_time - the global_time of a team
   * \param penalities_bonif - the penalities_bonif of a team 
   */
  Team(int number, std::string raid_code, std::string name, std::chrono::system_clock record_date, int ranking, int global_time,  int penalities_bonif); 

 // Destructive
  /**
   * \brief Default destructor of a team 
   */
   ~Team();

// Accessors (getters)
  /**
   * \brief Allows the recovery of the number 
   * \return int representing the number of a team
   */
   int getNumber();

  /**
   * \brief Allows the recovery of raid_code 
   * \return string representing the raid_code of a team
   */
   std::string getRaid_Code();

  /**
   * \brief Allows the recovery of the name 
   * \return string representing the name of a team
   */
   std::string getName();

  /**
   * \brief Allows the recovery of the record_date 
   * \return system_clock representing the record_date of a team
   */
   std::chrono::system_clock getRecord_Date();

  /**
   * \brief Allows the recovery of the ranking 
   * \return int representing the ranking of a team
   */
   int getRanking();

  /**
   * \brief Allows the recovery of the global_time 
   * \return int representing the global_time of a team
   */
   int getGlobal_Time();

  /**
   * \brief Allows the recovery of the penalites_bonif 
   * \return int representing the penalites_bonif of a team
   */
   int getPenalites_Bonif();
 
   
// Mutators (setters)
  /**                                               
   * \brief Allows the modification of the number           
   */
   void setNumber(int);

  /**  
   * \brief Allows the modification of the raid_code             
   */
   void setRaid_Code(std::string);

  /**                                                                                                                                       
   * \brief Allows the modification of the name              
   */
   void setName(std::string);

  /**
   * \brief Allows the modification of the record_date 
   */
   void setRecord_Date(std::chrono::system_clock);

  /**
   * \brief Allows the modification of the ranking            
   */
   void setRanking(int);

  /** 
   * \brief Allows the modification of the global_time
   */
   void setGlobal_Time(int);

  /**                                                                            
   * \brief Allows the modification of the penalites_bonif
   */
   void setPenalites_Bonif(int);
  
// Methods
  /**
   *  Allows the display of all parameters (attributes)
   */
  std::string toString();
};
#endif // TEAM_H
